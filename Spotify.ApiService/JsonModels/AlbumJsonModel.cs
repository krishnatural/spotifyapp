﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Spotify.ApiService.JsonModels
{
    public partial class AlbumJsonModel
    {
        [JsonProperty("albums")]
        public Albums Albums { get; set; }
    }

    public partial class Albums
    {
        [JsonProperty("href")]
        public Uri Href { get; set; }

        [JsonProperty("items")]
        public Item[] AlbumItems { get; set; }
    }

    public partial class Item
    {
        [JsonProperty("artists")]
        public List<Artist> Artists { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("images")]
        public List<Image> Images { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("release_date")]
        public string ReleaseDate { get; set; }
    }

    public partial class Artist
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public partial class Image
    {
        [JsonProperty("height")]
        public long Height { get; set; }

        [JsonProperty("url")]
        public Uri Url { get; set; }

        [JsonProperty("width")]
        public long Width { get; set; }
    }
}

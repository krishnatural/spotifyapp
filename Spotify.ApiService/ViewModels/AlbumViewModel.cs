﻿namespace Spotify.ApiService.ViewModels
{
    public class AlbumViewModel
    {
        public string AlbumName { get; set; }
        public string ArtistName { get; set; }
        public string ImageUrl { get; set; }
    }
}

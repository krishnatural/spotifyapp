﻿using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using Spotify.ApiService.Contracts;
using Spotify.ApiService.JsonModels;
using Spotify.ApiService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Spotify.ApiService.Services
{
    public class CatalogueApiService : ICatalogueApiService
    {
        public List<AlbumViewModel> GetAlbums(string query, string type)
        {
            var authToken = "BQCqTHU7bENJx7LWd9zQyXOwiK6CIf7oOCAcWOjlEdjaQJiUmDFOG9YS-LrYK_b2NSdW3cVbEtD5gW3LJUQLK1IUtkmxeWbxf1VRKxmg4pRTi8dRIMh88hfmk1vfn2vI6LN22niKRB5kd_ZGRqh2G4pdCpppWOK5K9GYC-lKv7lsWOuCSajHkljVOg";
            var restClient = new RestClient(new Uri("https://developer.spotify.com"));
            restClient.Authenticator = new JwtAuthenticator(authToken);
            var restRequest = new RestRequest("https://api.spotify.com/v1/search", Method.GET);
            restRequest.AddHeader("Content-type", "application/json");
            restRequest.AddParameter("q", query);
            restRequest.AddParameter("type", type);
            restRequest.AddParameter("limit", 50);
            var response = restClient.Execute(restRequest);
            if (response.IsSuccessful)
            {
                var albumViewModels = new List<AlbumViewModel>();
                var albumJsonModels = JsonConvert.DeserializeObject<AlbumJsonModel>(response.Content);
                foreach(var album in albumJsonModels.Albums.AlbumItems)
                {
                    var albumViewModel = new AlbumViewModel();
                    albumViewModel.AlbumName = album.Name;
                    albumViewModel.ArtistName = string.Join(", ", album.Artists.Select(a => a.Name));
                    albumViewModel.ImageUrl = album.Images.FirstOrDefault()?.Url.ToString();
                    albumViewModels.Add(albumViewModel);
                }
                return albumViewModels;
            }
            return null;
        }
    }
}

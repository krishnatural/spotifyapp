﻿using Spotify.ApiService.ViewModels;
using System.Collections.Generic;

namespace Spotify.ApiService.Contracts
{
    public interface ICatalogueApiService
    {
        List<AlbumViewModel> GetAlbums(string query, string type);
    }
}

﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Spotify.ApiService.Contracts;
using Spotify.ApiService.ViewModels;

namespace SpotifyApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CatalogueController : ControllerBase
    {
        ICatalogueApiService catalogueApiService;
        public CatalogueController(ICatalogueApiService catalogueApiService)
        {
            this.catalogueApiService = catalogueApiService;
        }

        // GET: api/Catalogue/5
        [HttpGet("{query}", Name = "Get")]
        public List<AlbumViewModel> Get(string query)
        {
            var type = "album";
            var albumViewModels = catalogueApiService.GetAlbums(query, type);
            return albumViewModels;
        }
    }
}

import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Lightbox } from 'ngx-lightbox';

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.css']
})

export class CatalogueComponent implements OnInit {
  public albums: Albums[];
  private lightAlbum: any;
  totalRec: number;
  page: number = 1;
  sortValue: any;
  sortOptions = ['Album Name', 'Artist Name'];
  constructor(private http: HttpClient, private _lightbox: Lightbox ) { }

  ngOnInit() {
  }

  getAlbums(searchValue) {
    this.http.get<Albums[]>('/api/Catalogue/' + searchValue).subscribe(result => {
      this.albums = result;
      if (this.albums != null) {
        this.totalRec = this.albums.length;
        this.page = 1;
        for (let i = 0; i < this.albums.length; i++) {
          const src = this.albums[i].imageUrl;
          const item = {
            src: src
          };

          this.lightAlbum.push(item);
        }
      }
    }, error => console.error(error));
  }

  openImage(index: number): void {
    // open lightbox
    this._lightbox.open(this.lightAlbum, index);
  }

  close(): void {
    // close lightbox programmatically
    this._lightbox.close();
  }

  sortAlbums() {
    if (this.sortValue != null) {
      if (this.albums != null) {
        if (this.sortValue == "Album Name") {
          this.albums.sort((a, b) => a.albumName.localeCompare(b.albumName));
        }
        else {
          this.albums.sort((a, b) => a.artistName.localeCompare(b.artistName));
        }
        this.page = 1;
      }
    }
  }
}
interface Albums {
  albumName: string;
  artistName: string;
  imageUrl: string;
}
